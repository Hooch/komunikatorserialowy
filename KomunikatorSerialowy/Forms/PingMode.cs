﻿using System;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO.Ports;

namespace KomunikatorSerialowy.Forms
{
	public partial class PingMode : Form
	{
		/// <summary>
		/// Ustawienie na true zapobiega wyłączeniu formatki.
		/// </summary>
		private bool preventClose = false;

		/// <summary>
		/// Ilość pakietów używanych do pingowania.
		/// </summary>
		private int pingPackets = Properties.Settings.Default.PingPacketsNumber;

		/// <summary>
		/// Konstruktor z portem.
		/// </summary>
		/// <param name="port">Aktywny port.</param>
		public PingMode(SerialPort port)
		{
			Port = port;
			Port.DataReceived += Port_DataReceived;
			InitializeComponent();
		}

		/// <summary>
		/// Typy pakietów.
		/// </summary>
		private enum PingDataType : byte
		{
			None,
			HostIdentification,
			AcceptHost,
			Prepare,
			Ready,
			PingData,
			Done
		}

		/// <summary>
		/// Aktywny port.
		/// </summary>
		public SerialPort Port { get; private set; }

		/// <summary>
		/// Obsługuje przychodzące dane.
		/// </summary>
		private void Port_DataReceived(object sender, SerialDataReceivedEventArgs e)
		{
			SerialPort port = (SerialPort)sender;
			byte[] allData = new byte[port.BytesToRead];
			port.Read(allData, 0, allData.Length);

			if (allData.Length != 1)
			{
				return;
			}

			byte data = allData[0];

			switch ((PingDataType)data)
			{
				case PingDataType.HostIdentification:
					SetWorkingStatusControls(true);
					Task.Factory.StartNew(() => StartAsSlave()).ContinueWith((t) => SetWorkingStatusControls(false));
					break;
				default:
					return;
			}
		}

		/// <summary>
		/// Rozpoczyna pracę w trybie host.
		/// </summary>
		private void StartAsHost()
		{
			System.Threading.Thread.CurrentThread.Priority = System.Threading.ThreadPriority.AboveNormal;

			Port.DataReceived -= Port_DataReceived;

			SetStatus("Indentyfing self as master...");

			Port.Write(PingData(PingDataType.HostIdentification), 0, 1);
			byte readByte = (byte)Port.ReadByte();

			SetStatus("Waiting for slave to accept...");

			if (readByte != (byte)PingDataType.AcceptHost)
				return;

			SetStatus("Sending prepare packet...");

			Port.Write(PingData(PingDataType.Prepare), 0, 1);

			SetStatus("Waiting for slave to be ready...");

			readByte = (byte)Port.ReadByte();
			if (readByte != (byte)PingDataType.Ready)
				return;

			SetStatus(String.Format("Pinging with {0} packets...", pingPackets));

			Stopwatch sw = new Stopwatch();
			sw.Start();

			byte[] pingData = PingData(PingDataType.PingData);

			for (int i = 0; i < pingPackets; i++)
			{
				Port.Write(pingData, 0, 1);
				readByte = (byte)Port.ReadByte();
				if (readByte != (byte)PingDataType.PingData)
					return;
			}

			sw.Stop();

			Port.Write(PingData(PingDataType.Done), 0, 1);

			SetStatus(String.Format("Sending results to slave"));

			long ticksNumber = sw.Elapsed.Ticks;
			ticksNumber = ticksNumber / pingPackets;

			byte[] ticks = BitConverter.GetBytes(ticksNumber);
			Port.Write(ticks, 0, ticks.Length);

			TimeSpan time = TimeSpan.FromTicks(ticksNumber);

			SetStatus(String.Format("RTT: {0} ms", time.TotalMilliseconds));

			Port.DataReceived += Port_DataReceived;
		}

		/// <summary>
		/// Rozpoczyna pracę w trybie slave.
		/// </summary>
		private void StartAsSlave()
		{
			System.Threading.Thread.CurrentThread.Priority = System.Threading.ThreadPriority.AboveNormal;

			Port.DataReceived -= Port_DataReceived;

			SetStatus("Accepted fate as slave.");

			Port.Write(PingData(PingDataType.AcceptHost), 0, 1);

			SetStatus("Waiting for host prepare signal...");

			byte readByte = (byte)Port.ReadByte();

			if (readByte != (byte)PingDataType.Prepare)
				return;

			SetStatus("Sending ready packet...");

			Port.Write(PingData(PingDataType.Ready), 0, 1);

			SetStatus("Responding for ping requests...");

			byte[] pingData = PingData(PingDataType.PingData);

			do
			{
				readByte = (byte)Port.ReadByte();

				switch ((PingDataType)readByte)
				{
					case PingDataType.PingData:
						Port.Write(pingData, 0, 1);
						break;
					case PingDataType.Done:
						break;
					default:
						return;
				}
			} while (readByte != (byte)PingDataType.Done);

			byte[] ticks = BitConverter.GetBytes((long)0);

			for (int i = 0; i < ticks.Length; i++)
			{
				SetStatus(String.Format("Waiting for results {0}/{1}...", i + 1, ticks.Length));
				ticks[i] = (byte)Port.ReadByte();
			}

			long tickNumber = BitConverter.ToInt64(ticks, 0);
			TimeSpan time = TimeSpan.FromTicks(tickNumber);

			SetStatus(String.Format("RTT: {0} ms", time.TotalMilliseconds));

			Port.DataReceived += Port_DataReceived;
		}

		/// <summary>
		/// Ustawia status.
		/// </summary>
		/// <param name="text">Tekst do ustawienia.</param>
		private void SetStatus(string text)
		{
			MethodInvoker m = () => labelStatus.Text = text;
			if(labelStatus.InvokeRequired)
			{
				labelStatus.Invoke(m);
			} else
			{
				m();
			}
		}

		/// <summary>
		/// Ustawia stan formatki w zależności od stanu pracy.
		/// </summary>
		/// <param name="isWorking">Czy ping jest w trakcie działania.</param>
		private void SetWorkingStatusControls(bool isWorking)
		{
			if(!isWorking)
				System.Threading.Thread.CurrentThread.Priority = System.Threading.ThreadPriority.Normal;

			MethodInvoker m = () =>
			{
				buttonStart.Enabled = !isWorking;
				preventClose = isWorking;
			};

			if (labelStatus.InvokeRequired)
			{
				labelStatus.Invoke(m);
			}
			else
			{
				m();
			}
		}

		/// <summary>
		/// Obsługa przycisku Start.
		/// </summary>
		private void buttonStart_Click(object sender, EventArgs e)
		{
			SetWorkingStatusControls(true);
			Task.Factory.StartNew(() => StartAsHost()).ContinueWith((t) => SetWorkingStatusControls(false));
		}

		/// <summary>
		/// Konwertuje typ pakietu na tablicę bytów.
		/// </summary>
		/// <param name="state">Typ pakietu.</param>
		/// <returns>Tablicę bytów.</returns>
		private byte[] PingData(PingDataType state)
		{
			return new byte[] { (byte)state };
		}

		/// <summary>
		/// Obsługuje zamknięcie formatki.
		/// </summary>
		private void PingMode_FormClosed(object sender, FormClosedEventArgs e)
		{
			Port.DataReceived -= Port_DataReceived;
		}

		/// <summary>
		/// Obsługuje zamykanie formatki.
		/// </summary>
		private void PingMode_FormClosing(object sender, FormClosingEventArgs e)
		{
			e.Cancel = preventClose;
		}
	}
}
