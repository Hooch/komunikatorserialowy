﻿using System;
using System.Diagnostics;
using System.Text;
using System.Windows.Forms;
using System.IO.Ports;

namespace KomunikatorSerialowy.Forms
{
	public partial class MainForm : Form
	{
		private Encoding receiveEncoding;
		private string appendString = String.Empty;

		public MainForm()
		{
			InitializeComponent();
			receiveTextBox.Text = String.Empty;
			sendTextBox.Enabled = false;
			receiveTextBox.Enabled = false;
			cbAppend.SelectedIndex = 0;
		}

		#region Transmisja

		/// <summary>
		/// Metoda wywoływana podczas wciśniecia przycisku "Send" z menu "Communication".
		/// Powoduje wysłanie danych
		/// </summary>
		private void sendToolStripMenuItem_Click(object sender, EventArgs e)
		{
			SendData();
			sendTextBox.Clear();
		}

		/// <summary>
		/// Metoda wywoływana podczas wciśniecia przycisku "Ping" z menu "Communication".
		/// Powoduje wykonanie pingowania drugiego urządzenia
		/// </summary>
		private void pingToolStripMenuItem_Click(object sender, EventArgs e)
		{
			if(!serialPort.IsOpen)
			{
				MessageBox.Show("Serial port must me in open state.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
				return;
			}

			serialPort.DataReceived -= serialPort_DataReceived;

			PingMode ping = new PingMode(serialPort);
			ping.ShowDialog();

			serialPort.DataReceived += serialPort_DataReceived;
		}

		private void SendData()
		{
			string text = String.Empty;

			text = sendTextBox.Text;

			SendData(text);
		}

		/// <summary>
		/// Wysyła tekst
		/// </summary>
		/// <param name="text">Tekst do wysłania</param>
		private void SendData(string text)
		{
			try
			{
				if (serialPort.IsOpen)
					serialPort.WriteLine(text + appendString);
			}
			catch (Exception ex)
			{
				if (ex is InvalidOperationException || ex is TimeoutException)
				{
					statusLabel.Text = "Not connected";

					sendTextBox.Enabled = false;
					receiveTextBox.Enabled = false;
					buttonSend.Enabled = false;

					MessageBox.Show("Serial port disconnected due to communication error.", "Error", MessageBoxButtons.OK,
						MessageBoxIcon.Error);
				}
				else
				{
					throw;
				}
			}
		}

		/// <summary>
		/// Akcja wywoływana w momencie odebrania danych z portu
		/// </summary>
		void serialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
		{
			SerialPort port = (SerialPort)sender;
			byte[] data = new byte[port.BytesToRead];
			port.Read(data, 0, data.Length);

			string text = receiveEncoding.GetString(data);

			MethodInvoker m = () => receiveTextBox.Text += text;
			if (receiveTextBox.InvokeRequired)
			{
				receiveTextBox.Invoke(m);
			}
			else
			{
				m();
			}
		}

		#endregion Transmisja

		#region Reszta Menu

		/// <summary>
		/// Metoda wywoływana podczas wciśniecia przycisku "Connect" z menu "COM".
		/// </summary>
		private void connectToolStripMenuItem_Click(object sender, EventArgs e)
		{
			SerialConfigureDialog dialog = new SerialConfigureDialog();

			if (!dialog.IsDisposed && dialog.ShowDialog() == DialogResult.OK)
			{
				if (serialPort != null && serialPort.IsOpen)
					serialPort.Close();

				serialPort = dialog.OpenedPort;
				receiveEncoding = dialog.ReceiveEncoding;

				sendTextBox.Clear();
				receiveTextBox.Clear();
				sendTextBox.Enabled = true;
				receiveTextBox.Enabled = true;
				buttonSend.Enabled = true;

				serialPort.DataReceived += serialPort_DataReceived;

				statusLabel.Text = "Connected: " + serialPort.PortName;
			}
		}

		/// <summary>
		/// Metoda wywoływana podczas wciśniecia przycisku "Disconnect" z menu "COM".
		/// </summary>
		private void disconnectToolStripMenuItem_Click(object sender, EventArgs e)
		{
			if (serialPort != null && serialPort.IsOpen)
				serialPort.Close();

			statusLabel.Text = "Not connected";

			sendTextBox.Enabled = false;
			receiveTextBox.Enabled = false;
			buttonSend.Enabled = false;
		}

		/// <summary>
		/// Metoda wywoływana podczas próby zamknięcia aplikacji przy pomocy menu "File".
		/// Zamyka aplikację
		/// </summary>
		private void exitToolStripMenuItem_Click(object sender, EventArgs e)
		{
			if (serialPort.IsOpen)
				serialPort.Close();

			Application.Exit();
		}

		#endregion Reszta Menu

		#region OtherActions

		/// <summary>
		/// Metoda wywoływana podczas zamykania aplikacji.
		/// Zamyka aplikację
		/// </summary>
		private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
		{
			if (serialPort.IsOpen)
				serialPort.Close();
		}

		#endregion OtherActions

		private void buttonSend_Click(object sender, EventArgs e)
		{
			SendData();
			sendTextBox.Clear();
		}

		private void buttonClean_Click(object sender, EventArgs e)
		{
			receiveTextBox.Clear();
		}

		private void cbAppend_SelectedIndexChanged(object sender, EventArgs e)
		{
			switch(cbAppend.SelectedIndex)
			{
				case 0:
					appendString = "\r\n";
					break;
				case 1:
					appendString = "\n";
					break;
				case 2:
					appendString = String.Empty;
					break;
			}
		}
	}
}
