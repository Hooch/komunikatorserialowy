﻿using System;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace KomunikatorSerialowy.Forms
{
    public partial class SerialConfigureDialog : Form
    {
        /// <summary>
        /// Otwarty port
        /// </summary>
        public SerialPort OpenedPort { get; private set; }

        /// <summary>
        /// Odbierane kodowanie
        /// </summary>
        public Encoding ReceiveEncoding { get; private set; }

        public SerialConfigureDialog()
        {
            InitializeComponent();
            InicializeData();
        }

        #region Inicialization

        /// <summary>
        /// Inicjalizuje danymi wszytskie kontrolki
        /// </summary>
        private void InicializeData()
        {
            InicializePortList();
            InicializeBaudRate();
            InicializeDataBits();
            InicializeHandshake();
            InicializePairty();
            InicializeStopBits();
            InicializeEncoding();
        }

        /// <summary>
        /// Inicjalizacja listy portów
        /// </summary>
        private void InicializePortList()
        {
            DialogResult noPortResult = DialogResult.OK;
            do
            {
                portsComboBox.Items.AddRange(SerialPort.GetPortNames());

                if (portsComboBox.Items.Count == 0)
                {
                    noPortResult = MessageBox.Show("No Serial ports found on this machine.", "Error",
                        MessageBoxButtons.RetryCancel,
                        MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);

                    if (noPortResult == DialogResult.Retry)
                        continue;

                    DialogResult = DialogResult.Cancel;
                    Close();
                }
            } while (noPortResult == DialogResult.Retry);

			if(portsComboBox.Items.Count > 0)
				portsComboBox.SelectedIndex = 0;
        }

        /// <summary>
        /// Inicjalizacja prędkość transmisji
        /// </summary>
        private void InicializeBaudRate()
        {
            baudRateComboBox.Items.AddRange(new object[]
	        {
	            75,
	            110,
	            134,
	            152,
	            300,
	            600,
	            1200,
	            1800,
	            2400,
	            4800,
	            7200,
	            9600,
	            14400,
	            19200,
	            38400,
	            57600,
	            115200,
	            128000
	        });
            baudRateComboBox.SelectedIndex = 11;
        }

        /// <summary>
        /// Inicjalizacja formatu znaku
        /// </summary>
        private void InicializeDataBits()
        {
            dataBits.Value = 8;
        }

        /// <summary>
        /// Inicjalizacja Handshake
        /// </summary>
        private void InicializeHandshake()
        {
            handshake.SelectedIndex = 0;
        }

        /// <summary>
        /// Inicjalizacja bitu parzystości
        /// </summary>
        private void InicializePairty()
        {
            parity.SelectedIndex = 0;
        }

        /// <summary>
        /// Inicjalizacja Bit stopu
        /// </summary>
        private void InicializeStopBits()
        {
            stopBits.SelectedIndex = 1;
        }

        /// <summary>
        /// Inicjalizacja Kodowanie
        /// </summary>
        private void InicializeEncoding()
        {
            encoding.Items.AddRange(GetEncodings());
            encoding.DisplayMember = "WebName";

            Encoding windowsEncoding = Encoding.GetEncoding("Windows-1252");
            encoding.SelectedItem = windowsEncoding;
        }

        /// <summary>
        /// Pobiera dostępne kodowania
        /// </summary>
        /// <returns>Lista dostępnych kodowań</returns>
        private object[] GetEncodings()
        {
            EncodingInfo[] encodingInfos = Encoding.GetEncodings();
            Encoding[] encodings = new Encoding[encodingInfos.Count()];

            for (int i = 0; i < encodings.Count(); i++)
            {
                encodings[i] = Encoding.GetEncoding(encodingInfos[i].Name);
            }

            return encodings;
        }

        #endregion Inicialization

        #region ToStringParesers

        /// <summary>
        /// Zwraca Handshake na podstawie dopasowania nazwy
        /// </summary>
        /// <param name="name">Nazwa Handshake</param>
        /// <returns>Handshake</returns>
        private Handshake HandshakeFromString(string name)
        {
            switch (name)
            {
                case "None":
                    return Handshake.None;
                case "XOnXOff":
                    return Handshake.XOnXOff;
                case "RequestToSend":
                    return Handshake.RequestToSend;
                case "RequestToSendXOnXOff":
                    return Handshake.RequestToSendXOnXOff;
                default:
                    throw new ArgumentException("No Handshake method named: " + name);
            }
        }

        /// <summary>
        /// Zwraca Parity na podstawie dopasowania nazwy
        /// </summary>
        /// <param name="name">Nazwa Parity</param>
        /// <returns>Parity</returns>
        private Parity ParityFromString(string name)
        {
            switch (name)
            {
                case "None":
                    return Parity.None;
                case "Odd":
                    return Parity.Odd;
                case "Even":
                    return Parity.Even;
                case "Mark":
                    return Parity.Mark;
                case "Space":
                    return Parity.Space;
                default:
                    throw new ArgumentException("No Parity method named: " + name);
            }
        }

        /// <summary>
        /// Zwraca StopBits na podstawie dopasowania nazwy
        /// </summary>
        /// <param name="name">Nazwa StopBits</param>
        /// <returns>StopBits</returns>
        private StopBits StopBitsFromString(string name)
        {
            switch (name)
            {
                case "None":
                    return StopBits.None;
                case "One":
                    return StopBits.One;
                case "Two":
                    return StopBits.Two;
                case "OnePointFive":
                    return StopBits.OnePointFive;
                default:
                    throw new ArgumentException("No StopBits method named: " + name);
            }
        }

        #endregion ToStringParesers

        #region ConnectButton

        /// <summary>
        /// Akcka wyzwalna podczas naciśnięcia przycisku "Connect" na formatce "COM Connect"
        /// </summary>
        private void connectButton_Click(object sender, EventArgs e)
        {
            SerialPort port = null;

            try
            {
                port = GetConfiguredPort();
                ReceiveEncoding = encoding.SelectedItem as Encoding;
                port.Open();
            }
            catch (Exception ex)
            {
                if (ex is UnauthorizedAccessException
                    || ex is ArgumentException
                    || ex is System.IO.IOException
                    || ex is InvalidOperationException)
                {
                    MessageBox.Show(ex.Message, "Error opening port", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    if (port != null && port.IsOpen)
                        port.Close();
                }
                else
                {
                    throw;
                }
            }

            OpenedPort = port;

            if (port == null)
            {
                return;
            }

            DialogResult = DialogResult.OK;
            Close();
        }

        /// <summary>
        /// Pobiera konfigurację z formularza.
        /// </summary>
        /// <returns>Konfiguracja z formularza</returns>
        public SerialPort GetConfiguredPort()
        {
            SerialPort port = new SerialPort
            {
                PortName = portsComboBox.SelectedItem.ToString(),
                BaudRate = (int)baudRateComboBox.SelectedItem,
                DataBits = (int)dataBits.Value,
                Handshake = HandshakeFromString(handshake.SelectedItem.ToString()),
                Parity = ParityFromString(parity.SelectedItem.ToString()),
                StopBits = StopBitsFromString(stopBits.SelectedItem.ToString())
            };

            return port;
        }

        #endregion ConnectButton
    }
}
