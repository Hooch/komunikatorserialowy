﻿namespace KomunikatorSerialowy.Forms
{
	partial class MainForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.sendTextBox = new System.Windows.Forms.TextBox();
			this.receiveTextBox = new System.Windows.Forms.TextBox();
			this.menuStrip = new System.Windows.Forms.MenuStrip();
			this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.comToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.connectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.disconnectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.communicationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.sendToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.pingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.terminalTable = new System.Windows.Forms.TableLayoutPanel();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.buttonSend = new System.Windows.Forms.Button();
			this.buttonClean = new System.Windows.Forms.Button();
			this.statusStrip1 = new System.Windows.Forms.StatusStrip();
			this.statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
			this.serialPort = new System.IO.Ports.SerialPort(this.components);
			this.cbAppend = new System.Windows.Forms.ToolStripComboBox();
			this.menuStrip.SuspendLayout();
			this.terminalTable.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.groupBox1.SuspendLayout();
			this.statusStrip1.SuspendLayout();
			this.SuspendLayout();
			// 
			// sendTextBox
			// 
			this.sendTextBox.AcceptsReturn = true;
			this.sendTextBox.AcceptsTab = true;
			this.sendTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
			this.sendTextBox.Location = new System.Drawing.Point(3, 16);
			this.sendTextBox.Multiline = true;
			this.sendTextBox.Name = "sendTextBox";
			this.sendTextBox.Size = new System.Drawing.Size(405, 434);
			this.sendTextBox.TabIndex = 0;
			// 
			// receiveTextBox
			// 
			this.receiveTextBox.AcceptsReturn = true;
			this.receiveTextBox.AcceptsTab = true;
			this.receiveTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
			this.receiveTextBox.Location = new System.Drawing.Point(3, 16);
			this.receiveTextBox.Multiline = true;
			this.receiveTextBox.Name = "receiveTextBox";
			this.receiveTextBox.ReadOnly = true;
			this.receiveTextBox.Size = new System.Drawing.Size(405, 434);
			this.receiveTextBox.TabIndex = 0;
			// 
			// menuStrip
			// 
			this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.comToolStripMenuItem,
            this.communicationToolStripMenuItem,
            this.cbAppend});
			this.menuStrip.Location = new System.Drawing.Point(0, 0);
			this.menuStrip.Name = "menuStrip";
			this.menuStrip.Size = new System.Drawing.Size(834, 27);
			this.menuStrip.TabIndex = 1;
			this.menuStrip.Text = "menuStrip1";
			// 
			// fileToolStripMenuItem
			// 
			this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
			this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
			this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 23);
			this.fileToolStripMenuItem.Text = "File";
			// 
			// exitToolStripMenuItem
			// 
			this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
			this.exitToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
			this.exitToolStripMenuItem.Text = "Exit";
			this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
			// 
			// comToolStripMenuItem
			// 
			this.comToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.connectToolStripMenuItem,
            this.disconnectToolStripMenuItem});
			this.comToolStripMenuItem.Name = "comToolStripMenuItem";
			this.comToolStripMenuItem.Size = new System.Drawing.Size(47, 23);
			this.comToolStripMenuItem.Text = "COM";
			// 
			// connectToolStripMenuItem
			// 
			this.connectToolStripMenuItem.Name = "connectToolStripMenuItem";
			this.connectToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
			this.connectToolStripMenuItem.Text = "Connect";
			this.connectToolStripMenuItem.Click += new System.EventHandler(this.connectToolStripMenuItem_Click);
			// 
			// disconnectToolStripMenuItem
			// 
			this.disconnectToolStripMenuItem.Name = "disconnectToolStripMenuItem";
			this.disconnectToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
			this.disconnectToolStripMenuItem.Text = "Disconnect";
			this.disconnectToolStripMenuItem.Click += new System.EventHandler(this.disconnectToolStripMenuItem_Click);
			// 
			// communicationToolStripMenuItem
			// 
			this.communicationToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sendToolStripMenuItem,
            this.pingToolStripMenuItem});
			this.communicationToolStripMenuItem.Name = "communicationToolStripMenuItem";
			this.communicationToolStripMenuItem.Size = new System.Drawing.Size(106, 23);
			this.communicationToolStripMenuItem.Text = "Communication";
			// 
			// sendToolStripMenuItem
			// 
			this.sendToolStripMenuItem.Name = "sendToolStripMenuItem";
			this.sendToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
			this.sendToolStripMenuItem.Text = "Send";
			this.sendToolStripMenuItem.Click += new System.EventHandler(this.sendToolStripMenuItem_Click);
			// 
			// pingToolStripMenuItem
			// 
			this.pingToolStripMenuItem.Name = "pingToolStripMenuItem";
			this.pingToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
			this.pingToolStripMenuItem.Text = "Ping";
			this.pingToolStripMenuItem.Click += new System.EventHandler(this.pingToolStripMenuItem_Click);
			// 
			// terminalTable
			// 
			this.terminalTable.ColumnCount = 2;
			this.terminalTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.terminalTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.terminalTable.Controls.Add(this.groupBox2, 1, 0);
			this.terminalTable.Controls.Add(this.groupBox1, 0, 0);
			this.terminalTable.Controls.Add(this.buttonSend, 0, 1);
			this.terminalTable.Controls.Add(this.buttonClean, 1, 1);
			this.terminalTable.Dock = System.Windows.Forms.DockStyle.Fill;
			this.terminalTable.Location = new System.Drawing.Point(0, 27);
			this.terminalTable.Name = "terminalTable";
			this.terminalTable.RowCount = 2;
			this.terminalTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.terminalTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
			this.terminalTable.Size = new System.Drawing.Size(834, 489);
			this.terminalTable.TabIndex = 2;
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.receiveTextBox);
			this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.groupBox2.Location = new System.Drawing.Point(420, 3);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(411, 453);
			this.groupBox2.TabIndex = 5;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Receive";
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.sendTextBox);
			this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.groupBox1.Location = new System.Drawing.Point(3, 3);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(411, 453);
			this.groupBox1.TabIndex = 4;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Send";
			// 
			// buttonSend
			// 
			this.buttonSend.Location = new System.Drawing.Point(3, 462);
			this.buttonSend.Name = "buttonSend";
			this.buttonSend.Size = new System.Drawing.Size(75, 23);
			this.buttonSend.TabIndex = 6;
			this.buttonSend.Text = "Send";
			this.buttonSend.UseVisualStyleBackColor = true;
			this.buttonSend.Click += new System.EventHandler(this.buttonSend_Click);
			// 
			// buttonClean
			// 
			this.buttonClean.Location = new System.Drawing.Point(420, 462);
			this.buttonClean.Name = "buttonClean";
			this.buttonClean.Size = new System.Drawing.Size(75, 23);
			this.buttonClean.TabIndex = 7;
			this.buttonClean.Text = "Clean";
			this.buttonClean.UseVisualStyleBackColor = true;
			this.buttonClean.Click += new System.EventHandler(this.buttonClean_Click);
			// 
			// statusStrip1
			// 
			this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusLabel});
			this.statusStrip1.Location = new System.Drawing.Point(0, 516);
			this.statusStrip1.Name = "statusStrip1";
			this.statusStrip1.Size = new System.Drawing.Size(834, 22);
			this.statusStrip1.TabIndex = 3;
			this.statusStrip1.Text = "statusStrip1";
			// 
			// statusLabel
			// 
			this.statusLabel.Name = "statusLabel";
			this.statusLabel.Size = new System.Drawing.Size(86, 17);
			this.statusLabel.Text = "Not connected";
			// 
			// cbAppend
			// 
			this.cbAppend.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbAppend.Items.AddRange(new object[] {
            "CR LF",
            "LF",
            "Nothing"});
			this.cbAppend.Name = "cbAppend";
			this.cbAppend.Size = new System.Drawing.Size(121, 23);
			this.cbAppend.SelectedIndexChanged += new System.EventHandler(this.cbAppend_SelectedIndexChanged);
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(834, 538);
			this.Controls.Add(this.terminalTable);
			this.Controls.Add(this.menuStrip);
			this.Controls.Add(this.statusStrip1);
			this.MainMenuStrip = this.menuStrip;
			this.Name = "MainForm";
			this.Text = "Komunikator Serialowy - GKiO2 - Sekcja 5";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
			this.menuStrip.ResumeLayout(false);
			this.menuStrip.PerformLayout();
			this.terminalTable.ResumeLayout(false);
			this.groupBox2.ResumeLayout(false);
			this.groupBox2.PerformLayout();
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.statusStrip1.ResumeLayout(false);
			this.statusStrip1.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.TextBox sendTextBox;
		private System.Windows.Forms.TextBox receiveTextBox;
		private System.Windows.Forms.MenuStrip menuStrip;
		private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem comToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem connectToolStripMenuItem;
		private System.Windows.Forms.TableLayoutPanel terminalTable;
		private System.Windows.Forms.StatusStrip statusStrip1;
		private System.Windows.Forms.ToolStripStatusLabel statusLabel;
		private System.IO.Ports.SerialPort serialPort;
		private System.Windows.Forms.ToolStripMenuItem disconnectToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem communicationToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ToolStripMenuItem sendToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pingToolStripMenuItem;
		private System.Windows.Forms.Button buttonSend;
		private System.Windows.Forms.Button buttonClean;
		private System.Windows.Forms.ToolStripComboBox cbAppend;
	}
}

