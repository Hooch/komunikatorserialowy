﻿namespace KomunikatorSerialowy.Forms
{
	partial class SerialConfigureDialog
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.portsComboBox = new System.Windows.Forms.ComboBox();
			this.label1 = new System.Windows.Forms.Label();
			this.baudRateComboBox = new System.Windows.Forms.ComboBox();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.dataBits = new System.Windows.Forms.NumericUpDown();
			this.label4 = new System.Windows.Forms.Label();
			this.handshake = new System.Windows.Forms.ComboBox();
			this.label5 = new System.Windows.Forms.Label();
			this.parity = new System.Windows.Forms.ComboBox();
			this.label6 = new System.Windows.Forms.Label();
			this.stopBits = new System.Windows.Forms.ComboBox();
			this.connectButton = new System.Windows.Forms.Button();
			this.label7 = new System.Windows.Forms.Label();
			this.encoding = new System.Windows.Forms.ComboBox();
			((System.ComponentModel.ISupportInitialize)(this.dataBits)).BeginInit();
			this.SuspendLayout();
			// 
			// portsComboBox
			// 
			this.portsComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.portsComboBox.FormattingEnabled = true;
			this.portsComboBox.Location = new System.Drawing.Point(97, 12);
			this.portsComboBox.Name = "portsComboBox";
			this.portsComboBox.Size = new System.Drawing.Size(130, 21);
			this.portsComboBox.TabIndex = 0;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(11, 15);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(57, 13);
			this.label1.TabIndex = 1;
			this.label1.Text = "Serial port:";
			// 
			// baudRateComboBox
			// 
			this.baudRateComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.baudRateComboBox.FormattingEnabled = true;
			this.baudRateComboBox.Location = new System.Drawing.Point(97, 39);
			this.baudRateComboBox.Name = "baudRateComboBox";
			this.baudRateComboBox.Size = new System.Drawing.Size(130, 21);
			this.baudRateComboBox.TabIndex = 2;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(11, 42);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(56, 13);
			this.label2.TabIndex = 1;
			this.label2.Text = "Baud rate:";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(12, 69);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(52, 13);
			this.label3.TabIndex = 1;
			this.label3.Text = "Data bits:";
			// 
			// dataBits
			// 
			this.dataBits.Location = new System.Drawing.Point(97, 67);
			this.dataBits.Maximum = new decimal(new int[] {
            8,
            0,
            0,
            0});
			this.dataBits.Minimum = new decimal(new int[] {
            5,
            0,
            0,
            0});
			this.dataBits.Name = "dataBits";
			this.dataBits.Size = new System.Drawing.Size(130, 20);
			this.dataBits.TabIndex = 3;
			this.dataBits.Value = new decimal(new int[] {
            8,
            0,
            0,
            0});
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(11, 96);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(65, 13);
			this.label4.TabIndex = 1;
			this.label4.Text = "Handshake:";
			// 
			// handshake
			// 
			this.handshake.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.handshake.FormattingEnabled = true;
			this.handshake.Items.AddRange(new object[] {
            "None",
            "XOnXOff",
            "RequestToSend",
            "RequestToSendXOnXOff"});
			this.handshake.Location = new System.Drawing.Point(97, 93);
			this.handshake.Name = "handshake";
			this.handshake.Size = new System.Drawing.Size(130, 21);
			this.handshake.TabIndex = 2;
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(11, 123);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(36, 13);
			this.label5.TabIndex = 1;
			this.label5.Text = "Pairty:";
			// 
			// parity
			// 
			this.parity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.parity.FormattingEnabled = true;
			this.parity.Items.AddRange(new object[] {
            "None",
            "Odd",
            "Even",
            "Mark",
            "Space"});
			this.parity.Location = new System.Drawing.Point(97, 120);
			this.parity.Name = "parity";
			this.parity.Size = new System.Drawing.Size(130, 21);
			this.parity.TabIndex = 2;
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(11, 150);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(51, 13);
			this.label6.TabIndex = 1;
			this.label6.Text = "Stop bits:";
			// 
			// stopBits
			// 
			this.stopBits.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.stopBits.FormattingEnabled = true;
			this.stopBits.Items.AddRange(new object[] {
            "None",
            "One",
            "Two",
            "OnePointFive"});
			this.stopBits.Location = new System.Drawing.Point(97, 147);
			this.stopBits.Name = "stopBits";
			this.stopBits.Size = new System.Drawing.Size(130, 21);
			this.stopBits.TabIndex = 2;
			// 
			// connectButton
			// 
			this.connectButton.Location = new System.Drawing.Point(154, 201);
			this.connectButton.Name = "connectButton";
			this.connectButton.Size = new System.Drawing.Size(75, 23);
			this.connectButton.TabIndex = 4;
			this.connectButton.Text = "Connect";
			this.connectButton.UseVisualStyleBackColor = true;
			this.connectButton.Click += new System.EventHandler(this.connectButton_Click);
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Location = new System.Drawing.Point(11, 177);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(55, 13);
			this.label7.TabIndex = 1;
			this.label7.Text = "Encoding:";
			// 
			// encoding
			// 
			this.encoding.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.encoding.FormattingEnabled = true;
			this.encoding.Location = new System.Drawing.Point(97, 174);
			this.encoding.Name = "encoding";
			this.encoding.Size = new System.Drawing.Size(130, 21);
			this.encoding.TabIndex = 2;
			// 
			// SerialConfigureDialog
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(241, 239);
			this.Controls.Add(this.connectButton);
			this.Controls.Add(this.dataBits);
			this.Controls.Add(this.encoding);
			this.Controls.Add(this.stopBits);
			this.Controls.Add(this.parity);
			this.Controls.Add(this.label7);
			this.Controls.Add(this.handshake);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.baudRateComboBox);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.portsComboBox);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Name = "SerialConfigureDialog";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "COM Connect";
			((System.ComponentModel.ISupportInitialize)(this.dataBits)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.ComboBox portsComboBox;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.ComboBox baudRateComboBox;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.NumericUpDown dataBits;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.ComboBox handshake;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.ComboBox parity;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.ComboBox stopBits;
		private System.Windows.Forms.Button connectButton;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.ComboBox encoding;
	}
}